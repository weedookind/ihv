import React from 'react';

class Output extends React.Component {
    render() {


        let row;
        let rows = [];

        for (let i = 0; i < this.props.value.length; i++) {

            let row = [];

            for (let j = 0; j < this.props.value[i].length; j++) {
                row.push(
                    <td>
                        {this.props.value[i][j]}
                    </td>
                );
            }
            rows.push(
                <tr>
                    {row}
                </tr>
            );
        }

        return (
            <table>
                {rows}
            </table>
        )
    }
}

export default Output;
