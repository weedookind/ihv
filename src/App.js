import React from 'react';
import InputField from './InputField';
import Output from "./Output";
import Button from "./Button";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            radikant: null,
            outputValue: []
        }
    }

    buttonClickHandler = () => {
        this.runIntervallHalbierung(0, 100)
    }

    inputChangeValueHandler = (value) => {
        this.setState({
            radikant: value
        })
    }

    runIntervallHalbierung = (a, b) => {

        let m, q;
        let out;

        let i = 0;

        let intervalHandler = window.setInterval(
            () => {
                m = (a + b) / 2;

                let out = this.state.outputValue;

                out.push([i, a, b, m]);

                this.setState({
                    outputValue: out,
                })

                q = m * m;


                if (q > this.state.radikant) {
                    b = m;
                } else {
                    a = m;
                }

                i++;

                if (i>10) {
                    window.clearInterval(intervalHandler);
                }
            }, 500
        )
    }

    render = () => {
        return (
            <div>
                Von welcher Zahl soll die Wurzel berechnet werden:
                <InputField
                    changeValueHandler={this.inputChangeValueHandler}
                />
                <Button
                    label={"los"}
                    color={"red"}
                    clickHandler={this.buttonClickHandler}
                />

                <br/>
                <br/>
                <Output
                    value={this.state.outputValue}
                />
            </div>
        )
    }
}

export default App;
