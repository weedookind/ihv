import React from 'react';

class Button extends React.Component {
    render() {
        let style = {
            display: "inline-block",
            border: "1px solid " + this.props.color,
            padding: "5px 10px",
            cursor: "pointer",
            color: this.props.color,
        }
        return (
            <div
                style={style}
                onClick={this.props.clickHandler}
            >
                {this.props.label}
            </div>
        )
    }
}

export default Button;
