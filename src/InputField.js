import React from 'react';

class InputField extends React.Component {
    render() {
        return (
            <input
                type={"number"}
                onChange={
                    (e) => {
                        this.props.changeValueHandler(e.target.value)
                    }
                }
            />
        )
    }
}

export default InputField;
